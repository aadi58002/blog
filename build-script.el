;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

;; Initialize the package system
(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; Install dependencies
(package-install 'htmlize)

;; Load the publishing system
(require 'ox-publish)

;; Customize the HTML output
(setq org-html-validation-link nil            ;; Don't show validation link
      org-html-head-include-scripts nil       ;; Use our own scripts
      org-html-head-include-default-style nil) ;; Use our own styles

;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list "org-site:Linux"
             :recursive t
             :base-extension "org"
             :base-directory "./Linux"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Linux/"
             :with-author t             ;; Don't include author name
             :with-creator t            ;; Include Emacs and Org versions in footer
             :with-toc t                ;; Include a table of contents
             :section-numbers nil       ;; Don't include section numbers
             :time-stamp-file t)
       (list "org-site:Linux-images"
             :recursive t
             :base-directory "./Linux/images"
             :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
             :publishing-function 'org-publish-attachment
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Linux/images/")
       (list "org-site:Anime-manga"
             :recursive t
             :base-directory "./Anime-manga"
             :base-extension "org"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Anime-manga/"
             :with-author t           ;; Don't include author name
             :with-creator t            ;; Include Emacs and Org versions in footer
             :with-toc t                ;; Include a table of contents
             :section-numbers nil       ;; Don't include section numbers
             :time-stamp-file t)
       (list "org-site:Anime-manga-images"
             :recursive t
             :base-directory "./Anime-manga/images"
             :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
             :publishing-function 'org-publish-attachment
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Anime-manga/images/")
       (list "org-site:Doom-emacs"
             :recursive t
             :base-directory "./Doom-emacs"
             :base-extension "org"
             :publishing-function 'org-html-publish-to-html
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Doom-emacs/"
             :with-author t           ;; Don't include author name
             :with-creator t            ;; Include Emacs and Org versions in footer
             :with-toc t                ;; Include a table of contents
             :section-numbers nil       ;; Don't include section numbers
             :time-stamp-file t)
       (list "org-site:Doom-emacs-images"
             :recursive t
             :base-directory "./Doom-emacs/images"
             :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
             :publishing-function 'org-publish-attachment
             :publishing-directory "~/Personal/Git-repos/Blog-website/pages/Doom-emacs/images/")
       (list "org-site:src"
             :recursive t
             :base-directory "./"
             :base-extension "css\\|js\\|setup"
             :publishing-function 'org-publish-attachment
             :publishing-directory "~/Personal/Git-repos/Blog-website/")
       ))

;; Generate the site output
(org-publish-all t)

(message "Build complete!")
